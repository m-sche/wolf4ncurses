# Makefile
# Wolf4ncurses
# Martin Scheubrein, 2013

CFLAGS = -Wall
OBJ = main.o draw.o sprites.o

wolf: $(OBJ)
	gcc $(CFLAGS) $(OBJ) -o $@ -lncurses -lm

sprites.o: sprites.c sprites.h
	gcc $(CFLAGS) sprites.c -c

main.o: main.c main.h draw.h defs.h
	gcc $(CFLAGS) main.c -c

draw.o: draw.c draw.h sprites.h defs.h
	gcc $(CFLAGS) draw.c -c

clean:
	rm -rf $(OBJ)

clobber:
	rm -rf *~ levels/*~ *.s gmon.out
