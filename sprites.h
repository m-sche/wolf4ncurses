/*
 * sprites.h
 * Sprites handling utilities of Wolf4ncurses
 *
 * Martin Scheubrein
 * 2013
 */

#include "defs.h"

int angle_to(int id, struct player_s player);
float dist_to(int id, struct player_s player);
