/*
 * main.h
 * Wolf4ncurses
 *
 * Martin Scheubrein
 * 2013
 */

#include "defs.h"

/* Command-line arguments (to save space) */
struct option_s {
	int show_maze;
	int round;
};

/* Administrative functions */
void init_ncurses(void);
void get_maze(const int round);

/* Primary gaming functions */
int play_game(struct option_s opt);
int player_move(void);
struct scrcol_s ray(const int angle);

/* Secondary gaming functions */
void wait_for(const char *string);

/* Auxiliary functions */
int char_match(const char var, const char *string);
int allocate_field(char ***adress, int rows, int cols);
int destroy_field(char ***adress, int rows);

#ifdef MODIFY_KBDRATE
void setx(void);
void resetx(void) __attribute__((destructor));
#endif

#define MSGLEN		(RESOLUTION-6)		/* Maximal message length */
#define WALKABLE	".123456789"		/* Free-to-go-through fields */
