/*
 * sprites.c
 * Sprites handling utilities of Wolf4ncurses
 *
 * Martin Scheubrein
 * 2013
 */

#include <math.h>

#include "defs.h"
#include "sprites.h"

struct player_s *ai;

/* Return angle to given sprite from local position */
int angle_to(int id, struct player_s player)
{
	/* Fucking math hack, FIXME! */
	return to_360(180*(player.row < ai[id].row)
		      -(int)(atan((player.col-ai[id].col)
		      /(player.row-ai[id].row))
		      /DEG_TO_RAD)-player.angle);
}

/* Return distance to given sprite from local position */
float dist_to(int id, struct player_s player)
{
	return sqrt(pow(player.row-ai[id].row, 2)
		    +pow(player.col-ai[id].col, 2));
}
