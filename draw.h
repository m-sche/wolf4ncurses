/*
 * draw.h
 * Drawing utilities of Wolf4ncurses
 *
 * Martin Scheubrein
 * 2013
 */

#include "defs.h"

/* Administrative functions */
void init_colors(void);

/* Primary drawing functions */
void draw_background(void);
void draw_screen(struct scrcol_s *scrbuf);
void draw_col(const struct scrcol_s col, int col_num, const char pattern[3]);

/* Secondary drawing functions */
void draw_maze(char **maze, int rows, int cols, struct player_s player);
void draw_player(int angle);
void draw_info(int round);
void draw_message(const char *message);
void draw_sprites(int max_id, struct player_s player, struct scrcol_s *scrbuf);
void draw_object(const int angle, const float distance);

#define WS_ROW getmaxy(stdscr)	/* Screen rows */
#define WS_COL getmaxx(stdscr)	/* Screen columns */

#define FATNESS	(WS_COL/RESOLUTION)	/* Screen cols per a degree */
#define WIDTH	(RESOLUTION*FATNESS)	/* Used screen width */

/* Useful for color index handling */
#define BLACK	1
#define WHITE	2
#define CYAN	5
#define GREEN	8
#define BLUE	11
#define YELLOW	14
#define RED	17

#define DARK(color)	(color+1)
#define DEEP(color)	(color+2)
