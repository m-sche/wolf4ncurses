/*
 * defs.h
 * Global definitions of Wolf4ncurses
 *
 * Martin Scheubrein
 * 2013
 */

#ifndef WOLF4NCURSES_DEFS
#define WOLF4NCURSES_DEFS

#define MAJOR_VERSION	1
#define MINOR_VERSION	1

/* You can change these few macros to modify some game attributes */

#define RESOLUTION	80	/* Visible degrees (360 is funny) */
#define STEP		0.08	/* Forward step in block units */
#define ROTATION	4	/* Rotation step in degrees */
#define ACCURACY	0.05	/* Accuracy of distance measurment */
#define BLOW		10	/* Relative height/width ratio */

#define BACKGROUND	'%'	/* Background texture */

#define ERR_FILE	10	/* File operation failure code */
#define ERR_ALLOC	11	/* Memory allocating failure code */
#define ERR_TERM	12	/* Terminal failure code */
#ifndef	EXIT_SUCCESS
  #define EXIT_SUCCESS	0	/* Successful quit code */
#endif

/* If you want wolf4ncurses to modify keyboard repeat rate and delay for better
 * playability, uncomment following #define. But before compiling, look at
 * output of command 'xset q' and change constants KBD_RATE and KBD_DELAY to the
 * values 'xset q' returned. These are the values of keyboard repeat rate and
 * delay, which will be set when you terminate the program.
 */

#define MODIFY_KBDRATE

#ifdef MODIFY_KBDRATE
#define KBD_RATE	33
#define KBD_DELAY	300
#endif

/*
 * Following definitions doesn't affect playability, change only if you know
 * what you are doing and think before doing so.
 */

#define DEG_TO_RAD	0.0174533L		/* Degrees in a radian */
#define to_360(angle)	((360+(angle))%360)	/* Get an valid angle */

#define eprintf(...)	fprintf(stderr, __VA_ARGS__)	/* Error message */
#define chtoi(ch)	((ch)-'0')	/* Convert char digit to int */

/* Structure describing human players state */
struct player_s {
	int angle;
	float row;
	float col;
};

/* Screen column parametres */
struct scrcol_s {
	float dist;
	int color;
};

#endif
