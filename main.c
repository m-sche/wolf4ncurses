/*
 * main.c
 * Wolf4ncurses
 *
 * Martin Scheubrein
 * 2013
 */

#include <stdio.h>
#include <stdlib.h>
#include <ncurses.h>
#include <math.h>
#include <ctype.h>
#include <unistd.h>

#include "defs.h"
#include "main.h"
#include "draw.h"

static int rows;
static int cols;
static char **maze;
static struct player_s player = {90, 0, 0};
static int max_id;

extern struct player_s *ai;	/* From sprites.c */

int main(int argc, char *argv[])
{
	/* {show_maze, round} */
	struct option_s opt = {0, 1};
	int flag;

	/* Parse options */
	while ((flag = getopt(argc, argv, "hmr:v")) != -1) {
		switch (flag) {
		case '?':
		case 'h':
			printf("Wolf4ncurses -- terminal 3D engine\n"
			       "Usage:\n%s [OPTIONS]\nOPTIONS:\n"
			       "  -h\t\tPrint this help box and quit\n"
			       "  -m\t\tDiplay map on the screen\n"
			       "  -r ROUND\tStart at ROUNDth round\n"
			       "  -v\t\tPrint version number and quit\n"
			       "Copyright (c) 2013 Martin Scheubrein\n",
			       argv[0]);
			exit(EXIT_SUCCESS);
		case 'm':
			opt.show_maze = 1;
			break;
		case 'r':
			opt.round = atoi(optarg);
			break;
		case 'v':
			printf("wolf4ncurses %d.%d\n",
			       MAJOR_VERSION, MINOR_VERSION);
			exit(EXIT_SUCCESS);
		}
	}

	/* Game */
	get_maze(opt.round);
	init_ncurses();
#ifdef MODIFY_KBDRATE
	setx();
#endif
	while (play_game(opt)) {
		endwin();
		get_maze(++opt.round);
		player.angle = 90;
		init_ncurses();
	}

	destroy_field(&maze, rows);
	endwin();

	return EXIT_SUCCESS;
}

void init_ncurses(void)
{
	initscr();

	if (!has_colors()) {
		endwin();
		eprintf("Your terminal doesn't support colors\n");
		initscr();
	}

	int scr_width = getmaxx(stdscr);

	if (scr_width < RESOLUTION) {
		endwin();
		eprintf("Minimal screen width is %d, you have %d\n",
			RESOLUTION, scr_width);
		exit(ERR_TERM);
	}

	curs_set(0);
	noecho();
	keypad(stdscr, TRUE);

	start_color();
	init_colors();
}

/* Load maze into array */
void get_maze(const int round)
{
	char maze_name[18] = "levels/maze??.txt";
	FILE *mazefile;

	maze_name[11] = round/10+'0';
	maze_name[12] = round%10+'0';

	/* Bureaucracy... */
	mazefile = fopen(maze_name, "r");
	if (mazefile == NULL) {
		eprintf("Can't open %s\n", maze_name);
		exit(ERR_FILE);
	}

	if (fscanf(mazefile, "%d %d", &rows, &cols) != 2) {
		eprintf("Bad format of maze.txt\n");
		exit(ERR_FILE);
	}
	getc(mazefile);

	if (allocate_field(&maze, rows, cols) == ERR_ALLOC) {
		eprintf("Not enough memory\n");
		exit(ERR_ALLOC);
	}

	/* Load maze into array */

	int row, col;
	int begin;

	begin = ftell(mazefile);
	for (row = 0; row < rows; row++) {
		for (col = 0; col < cols; col++) {
			maze[row][col] = getc(mazefile);
			switch (maze[row][col]) {
			case '*':
				maze[row][col] = '.';
				player.row = row+0.5;
				player.col = col+0.5;
				break;
			case 's':
				max_id++;
				break;
			default:
				break;
			}
		}
		getc(mazefile);
	}

	/* Load sprites */

	int id = 0;

	ai = (struct player_s *)malloc(max_id*sizeof(struct player_s));
	if (ai == NULL) {
		eprintf("Not enough memory\n");
		exit(ERR_ALLOC);
	}
	fseek(mazefile, begin, SEEK_SET);
	for (row = 0; row < rows; row++) {
		for (col = 0; col < cols; col++) {
			if (getc(mazefile) == 's') {
				maze[row][col] = '.';
				ai[id].row = row+0.5;
				ai[id].col = col+0.5;
				id++;
			}
		}
		getc(mazefile);
	}

	fclose(mazefile);
}

/* Play a game cycle in a single maze, return 1 if player wants to continue */
int play_game(struct option_s opt)
{
	int column;
	struct scrcol_s scrbuf[RESOLUTION];

	clear();
	while (1) {
		if (player.row < -0.5 || player.row > rows-0.5 ||
		    player.col < 0.5 || player.col > cols-0.5){
			draw_message("You escaped!");
			while (1) {
				switch (toupper(getch())) {
				case 'Q':
					return 0;
				case KEY_LEFT:
				case KEY_RIGHT:
				case KEY_UP:
				case KEY_DOWN:
				case 'I' ... 'L':	/* Abracadabra! */
					break;
				default:
					destroy_field(&maze, rows);
					return 1;
				}
			}
		}

		player.angle = to_360(player.angle);	/* Prevent overflow */

		/* Fill screen buffer */
		for (column = 0; column < RESOLUTION; column++)
			scrbuf[column] = ray(column-RESOLUTION/2+player.angle);

		draw_background();
		draw_screen(scrbuf);
		draw_sprites(max_id, player, scrbuf);

		if (opt.show_maze)
			draw_maze(maze, rows, cols, player);
		draw_info(opt.round);

		if (!player_move())
			return 0;
	}
}

/*
 * Get user input and react. If a forward/backward key is pressed, check if
 * final destination is free, else you are in front of wall so check if you
 * can walk at least along the wall.
 */
int player_move(void)
{
	const float row_step = -STEP*cos(DEG_TO_RAD*player.angle);
	const float col_step = STEP*sin(DEG_TO_RAD*player.angle);
	const int fwd_row = player.row+row_step;
	const int fwd_col = player.col+col_step;
	const int bwd_row = player.row-row_step;
	const int bwd_col = player.col-col_step;

	switch (toupper(getch())) {
	case 'Q':
		return 0;
	case 'J':
	case KEY_LEFT:
		player.angle -= ROTATION;
		break;
	case 'L':
	case KEY_RIGHT:
		player.angle += ROTATION;
		break;
	case 'I':
	case KEY_UP:
		if (char_match(maze[fwd_row][fwd_col], WALKABLE)) {
			player.row += row_step;
			player.col += col_step;
		} else if (char_match(maze[fwd_row][(int)player.col], WALKABLE)) {
			player.row += row_step;
		} else if (char_match(maze[(int)player.row][fwd_col], WALKABLE)) {
			player.col += col_step;
		}
		break;
	case 'K':
	case KEY_DOWN:
		if (char_match(maze[bwd_row][bwd_col], WALKABLE)) {
			player.row -= row_step;
			player.col -= col_step;
		} else if (char_match(maze[bwd_row][(int)player.col], WALKABLE)) {
			player.row -= row_step;
		} else if (char_match(maze[(int)player.row][bwd_col], WALKABLE)) {
			player.col -= col_step;
		}
		break;
	default:
		return 1;
	}
	return 1;
}

/* Get parameters of nearest object in given angle; if none, return black */
struct scrcol_s ray(const int angle)
{
	int row = player.row;
	int col = player.col;
	struct scrcol_s column = {0, 0};

	while (row >= 0 && col >= 0 && row < rows && col < cols) {
		switch (maze[row][col]) {
		case 'c':
			column.color = CYAN;
			return column;
		case 'g':
			column.color = GREEN;
			return column;
		case 'b':
			column.color = BLUE;
			return column;
		case 'y':
			column.color = YELLOW;
			return column;
		case 'r':
			column.color = RED;
			return column;
		default:
			column.dist += ACCURACY;
			row = player.row-(column.dist*cos(DEG_TO_RAD*angle));
			col = player.col+(column.dist*sin(DEG_TO_RAD*angle));
			break;
		}
	}

	column.dist = 0;
	column.color = 1;
	return column;
}

/* Stop game until player presses one of characters from string */
void wait_for(const char *string)
{
	while (!char_match((char)getch(), string))
		;
}

/* Compares var to all characters from string */
int char_match(const char var, const char *string)
{
	while (*string != '\0') {
		if (*string == var)
			return 1;
		string++;
	}
	return 0;
}

/* Allocate 2D array at given adress */
int allocate_field(char ***adress, int rows, int cols)
{
	*adress = (char **)malloc(rows*sizeof(char *));
	if (*adress == NULL)
		return ERR_ALLOC;

	while (rows--) {
		(*adress)[rows] = (char *)malloc(cols*sizeof(char));
		if ((*adress)[rows] == NULL)
			return ERR_ALLOC;
	}

	return EXIT_SUCCESS;
}

/* Destroy 2D array at given adress */
int destroy_field(char ***adress, int rows)
{
	while (rows--)
		free((*adress)[rows]);

	free(*adress);
	*adress = NULL;

	return EXIT_SUCCESS;
}

#ifdef MODIFY_KBDRATE

/* Set keyboard repeat rate to optimal values */
void setx(void)
{
	system("xset r rate 50 25 2>/dev/null");
}

/* Set keyboard repeat rate to normal values (__attribute__ destructor) */
void resetx(void)
{
	char command[21];	/* Enough for 10s & 1kHz */

	sprintf(command, "xset r rate %d %d", KBD_DELAY, KBD_RATE);
	system(command);
}

#endif /* MODIFY_KBDRATE */
