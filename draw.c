/*
 * draw.c
 * Drawing utilities of Wolf4ncurses
 *
 * Martin Scheubrein
 * 2013
 */

#include <stdio.h>
#include <string.h>
#include <ncurses.h>

#include "defs.h"
#include "draw.h"
#include "sprites.h"

/* Initialize colors */
void init_colors(void)
{
	init_pair(BLACK,	COLOR_BLACK, COLOR_BLACK);

	init_pair(WHITE,	COLOR_WHITE, COLOR_WHITE);
	init_pair(DARK(WHITE),	COLOR_BLACK, COLOR_WHITE);
	init_pair(DEEP(WHITE),	COLOR_WHITE, COLOR_BLACK);

	init_pair(CYAN,		COLOR_CYAN, COLOR_CYAN);
	init_pair(DARK(CYAN),	COLOR_BLACK, COLOR_CYAN);
	init_pair(DEEP(CYAN),	COLOR_CYAN, COLOR_BLACK);

	init_pair(GREEN,	COLOR_GREEN, COLOR_GREEN);
	init_pair(DARK(GREEN),	COLOR_BLACK, COLOR_GREEN);
	init_pair(DEEP(GREEN),	COLOR_GREEN, COLOR_BLACK);

	init_pair(BLUE,		COLOR_BLUE, COLOR_BLUE);
	init_pair(DARK(BLUE),	COLOR_BLACK, COLOR_BLUE);
	init_pair(DEEP(BLUE),	COLOR_BLUE, COLOR_BLACK);

	init_pair(YELLOW,	COLOR_YELLOW, COLOR_YELLOW);
	init_pair(DARK(YELLOW),	COLOR_BLACK, COLOR_YELLOW);
	init_pair(DEEP(YELLOW),	COLOR_YELLOW, COLOR_BLACK);

	init_pair(RED,		COLOR_RED, COLOR_RED);
	init_pair(DARK(RED),	COLOR_BLACK, COLOR_RED);
	init_pair(DEEP(RED),	COLOR_RED, COLOR_BLACK);
}

/* Draw background fading to black */
void draw_background(void)
{
	int row, col;
	const int col_margin = (WS_COL-WIDTH)/2;
	const int ws_row = WS_ROW;
	const int width = WIDTH;

	for (row = 0; row <= ws_row/2; row++) {
		if (row < ws_row*0.15)
			attron(COLOR_PAIR(WHITE));
		else if (row < ws_row*0.3)
			attron(COLOR_PAIR(DARK(WHITE)));
		else if (row < ws_row*0.4)
			attron(COLOR_PAIR(DEEP(WHITE)));
		else
			attron(COLOR_PAIR(BLACK));

		move(row, col_margin);
		for (col = 0; col < width; col++)
			addch(BACKGROUND);
		move(ws_row-row-1, col_margin);
		for (col = 0; col < width; col++)
			addch(BACKGROUND);
	}
	attroff(COLOR_PAIR(BLACK));
	attroff(COLOR_PAIR(WHITE));
	attroff(COLOR_PAIR(DARK(WHITE)));
	attroff(COLOR_PAIR(DEEP(WHITE)));
}

/* Draw walls */
void draw_screen(struct scrcol_s *scrbuf)
{
	int col;

	for (col = 0; col < RESOLUTION; col++) {
		if (scrbuf[col].dist > 5.4) {
			scrbuf[col].color = BLACK;
			draw_col(scrbuf[col], col, "  ");
		} else if (scrbuf[col].dist > 4.1) {
			scrbuf[col].color = DEEP(scrbuf[col].color);
			draw_col(scrbuf[col], col, "+ ");
		} else if (scrbuf[col].dist > 2.9) {
			scrbuf[col].color = DEEP(scrbuf[col].color);
			draw_col(scrbuf[col], col, "##");
		} else if (scrbuf[col].dist > 1.8) {
			scrbuf[col].color = DARK(scrbuf[col].color);
			draw_col(scrbuf[col], col, "##");
		} else if (scrbuf[col].dist > 1.0) {
			scrbuf[col].color = DARK(scrbuf[col].color);
			draw_col(scrbuf[col], col, "+ ");
		} else {
			draw_col(scrbuf[col], col, "  ");
		}
	}
}

/* Draw a column of the wall */
void draw_col(const struct scrcol_s col, int col_num, const char pattern[3])
{
	int height;
	int size;

	const int col_margin = (WS_COL-WIDTH)/2;
	const int row_margin = WS_ROW/2;
	const int fatness = FATNESS;
	const int max_height = (int)(BLOW*fatness/col.dist);

	attron(COLOR_PAIR(col.color));
	for (height = 0; height < max_height; height++) {
		for (size = 0; size < fatness; size++) {
			mvaddch(row_margin+height,
				col_margin+col_num*fatness+size,
				pattern[size%2]);
			mvaddch(row_margin-height,
				col_margin+col_num*fatness+size,
				pattern[size%2]);
		}
	}
	attroff(COLOR_PAIR(col.color));
}

/* Draw sprites */
void draw_sprites(int max_id, struct player_s player, struct scrcol_s *scrbuf)
{
	int id;
	int spr_angle;
	float spr_dist;
	struct scrcol_s spritebuf[RESOLUTION];

	memset(spritebuf, 0, RESOLUTION*sizeof(struct scrcol_s));

	for (id = 0; id < max_id; id++) {
		spr_angle = angle_to(id, player);
		spr_angle = spr_angle > 180 ? spr_angle-360 : spr_angle;
		spr_dist = dist_to(id, player);

		if (spr_angle >= -RESOLUTION/2 && spr_angle <= RESOLUTION/2) {
			mvprintw(0, WS_COL/2+FATNESS*spr_angle, "V");
			if (spritebuf[spr_angle+RESOLUTION/2].dist == 0 ||
			spr_dist < spritebuf[spr_angle+RESOLUTION/2].dist)
				spritebuf[spr_angle+RESOLUTION/2].dist = spr_dist;
		}
	}

	for (spr_angle = 0; spr_angle < RESOLUTION; spr_angle++) {
		if (spritebuf[spr_angle].dist && (scrbuf[spr_angle].dist == 0 ||
		spritebuf[spr_angle].dist < scrbuf[spr_angle].dist))
			draw_object(spr_angle-RESOLUTION/2, spritebuf[spr_angle].dist);
	}
}

/* Draw an object (sprite) */
void draw_object(const int angle, const float distance)
{
	int height = 0;
	int size = 0;

	const int margin = WS_ROW/2;

	attron(COLOR_PAIR(DARK(RED)));
	for (height = 0; height < BLOW*FATNESS/distance; height++) {
		for (size = 0; size < FATNESS; size++) {
			mvaddch(margin-height/2, WS_COL/2+FATNESS*angle+size, '^');
			mvaddch(margin+height, WS_COL/2+FATNESS*angle+size, 'v');
		}
	}
	attroff(COLOR_PAIR(DARK(RED)));
}

/* Draw map of maze in a corner of the screen */
void draw_maze(char **maze, int rows, int cols, struct player_s player)
{
	int row, col;

	for (row = 0; row < rows; row++) {
		move(row+1, 0);
		for (col = 0; col < cols; col++) {
			if ((int)player.row == row && (int)player.col == col)
				draw_player(player.angle);
			else
				printw(" %c", maze[row][col]);
		}
	}
}

/* Draw player's character in maze */
void draw_player(int angle)
{
	if (angle%360 < 45)
		addstr(" ^");
	else if (angle%360 < 135)
		addstr(" >");
	else if (angle%360 < 235)
		addstr(" v");
	else if (angle%360 < 315)
		addstr(" <");
	else
		addstr(" ^");
}

/* Draw info about current game */
void draw_info(int round)
{
	mvprintw(WS_ROW-2, (WS_COL-WIDTH)/2, "~~~~~~~~~~\\");
	mvprintw(WS_ROW-1, (WS_COL-WIDTH)/2, "Round: %2d |", round);
}

/* Draw victory message (not used yet) */
void draw_message(const char *message)
{
	int i;
	const int col = (WS_COL-strlen(message))/2;

	attron(A_REVERSE);
	mvprintw(WS_ROW/2, col, "| %s |", message);
	for (i = 0; i < strlen(message)+4; i++) {
		mvaddch(WS_ROW/2-2, col+i, '=');
		mvaddch(WS_ROW/2-1, col+i, ' ');
		mvaddch(WS_ROW/2+1, col+i, ' ');
		mvaddch(WS_ROW/2+2, col+i, '=');
	}
	mvaddch(WS_ROW/2-1, col, '|');
	mvaddch(WS_ROW/2-1, col+strlen(message)+3, '|');
	mvaddch(WS_ROW/2+1, col, '|');
	mvaddch(WS_ROW/2+1, col+strlen(message)+3, '|');
	attroff(A_REVERSE);
}
